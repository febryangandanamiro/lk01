import java.util.ArrayList;
public class Kereta {
    // Atribut
    private String namaKereta;
    private int jumlahTiketKomuter;
    private int jumlahTiketKJJ;
    private ArrayList<Tiket> daftarTiket;

    //constructor
    public Kereta(){
    this.namaKereta="Komuter";
    this.jumlahTiketKomuter=1000;
    daftarTiket = new ArrayList<Tiket>();
    }
    public Kereta(String namaKereta, int jumlahTiketKJJ){
    this.namaKereta=namaKereta;
    this.jumlahTiketKJJ=jumlahTiketKJJ;
    daftarTiket = new ArrayList<Tiket>();
    }
    public void tambahTiket(String namaPenumpang) {
            if(jumlahTiketKomuter > 0) {
            Tiket tiket = new Tiket(namaPenumpang);
            daftarTiket.add(tiket);
            jumlahTiketKomuter--;
            System.out.println("=======================================================================");
            System.out.println("Tiket berhasil dipesan");
            if(jumlahTiketKomuter < 30) {
                System.out.println("Sisa tiket tersedia : " + jumlahTiketKomuter);
            }
        } else {
            System.out.println("=======================================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya.");
        }}

    public void tambahTiket(String namaPenumpang, String asal, String tujuan) {
            if(jumlahTiketKJJ > 0) {
                Tiket tiket = new Tiket(namaPenumpang, asal, tujuan);
                daftarTiket.add(tiket);
                jumlahTiketKJJ--;
                System.out.println("=======================================================================");
                System.out.print("Tiket berhasil dipesan. ");
                if(jumlahTiketKJJ < 30) {
                    System.out.println("Sisa tiket tersedia : " + jumlahTiketKJJ);
                }
            } else {
                System.out.println("=======================================================================");
                System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya.");
            }
        }
    public void tampilkanTiket() {
        System.out.println("=======================================================================");
        System.out.println("Daftar penumpang kereta api " + this.namaKereta + " :");
        for (Tiket tiket : this.daftarTiket) {
            if (this.namaKereta.equals("Komuter")) {
                System.out.println("Nama : " + tiket.getNamaPenumpang());
            } else {
                System.out.println("--------------------------\nNama : " + tiket.getNamaPenumpang());
                System.out.println("Asal : " + tiket.getAsal());
                System.out.println("Tujuan : "+tiket.getTujuan());
            }
        }
    }
}