
public class Tiket {
    // atribut
    private String namaPenumpang;
    private String asal;
    private String tujuan;

    //method
    public String getNamaPenumpang() {
        return namaPenumpang;
    }
    public String getAsal(){
        return asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    //constructor
    public Tiket(){}
    public Tiket(String namaPenumpang){
        this.namaPenumpang=namaPenumpang;
    }
    public Tiket(String namaPenumpang, String asal, String tujuan){
        this.namaPenumpang=namaPenumpang;
        this.asal=asal;
        this.tujuan=tujuan;
    }


}